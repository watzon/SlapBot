const path = require('path');
const environment = require('../utils/environment');
if (environment == 'development') require('dotenv').config();

module.exports = exports = {
    development: {
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
      host: '127.0.0.1',
      dialect: 'sqlite',
      storage: path.resolve('src', 'db', 'data.sqlite')
    },
    test: {
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
      host: '127.0.0.1',
      dialect: 'postgresql'
    },
    production: {
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
      host: '127.0.0.1',
      dialect: 'postgresql'
    }
  }