const Sequelize = require('sequelize');
const config = require('./database');
const environment = require('../utils/environment');
if (environment == 'development') require('dotenv').config();

module.exports = exports = new Sequelize(config[environment]);