exports.help = require('./help');
exports.slap = require('./slap');
exports.slapcount = require('./slapcount');
exports.disableslaps = require('./disableslaps');
exports.enableslaps = require('./enableslaps');
exports.slappermissions = require('./slappermissions');