const makeMention = require('../utils/makeMention');
const getTarget = require('../utils/getTarget');
const isAdmin = require('../utils/isAdmin');
const { isDisabled, makeDisabled } = require('../utils/admin');
const Slap = require('../db/models').Slap;

module.exports =  {
    command: ['/slappermissions'],
    action: async (bot, msg, self, details) => {
        let target = getTarget(msg);
        let disabled = await isDisabled(msg.chat.id, target);
        let message = disabled ? `Slapping has been disabled for ${target}.` : `Slapping is enabled for ${target}`;

        bot.sendMessage(msg.chat.id, message);
    }
}