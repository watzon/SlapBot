const makeMention = require('../utils/makeMention');
const getTarget = require('../utils/getTarget');
const isAdmin = require('../utils/isAdmin');
const { makeDisabled } = require('../utils/admin');

module.exports =  {
    command: ['/disableslaps'],
    action: async (bot, msg, self, details) => {
        let target = getTarget(msg);
        let user = makeMention(msg.from.username);

        let admin = await isAdmin(bot, msg, msg.from.username)

        if (admin) {
            let disabled = await makeDisabled(msg.chat.id, target);
            bot.sendMessage(msg.chat.id, `${user} has suspended the slapping priveleges of ${target}.`);
        } else {
            bot.sendMessage(msg.chat.id, `I can't allow that, ${user}.`);
        }
    }
}