const makeMention = require('../utils/makeMention');
const getTarget = require('../utils/getTarget');
const isAdmin = require('../utils/isAdmin');
const probe = require('pmx').probe();
const { isDisabled, makeDisabled } = require('../utils/admin');
const Slap = require('../db/models').Slap;

const counter = probe.counter({
    name: 'Slap count'
});

module.exports =  {
    command: ['/slap', '/pleaseslap'],
    action: async (bot, msg, self, details) => {
        let target = getTarget(msg);
        let user = makeMention(msg.from.username);
        let disabled = await isDisabled(msg.chat.id, user);

        if (disabled) {
            bot.sendMessage(msg.from.id, `Your slapping privileges have been suspended.`)
            return;
        }

        isAdmin(bot, msg).then(admin => {
            if (admin && admin.can_delete_messages) {
                bot.deleteMessage(msg.chat.id, msg.message_id);
            }
        });

        if (target === makeMention(msg.from.username)) {
            bot.sendMessage(msg.chat.id, `Why are you hitting yourself, ${user}?`);
        } else if (target === '@SuperSlapBot') {
            bot.sendMessage(msg.chat.id, `Why would you hit me, ${user}? I thought we were friends.`);
        } else {
            Slap.create({ chatId: msg.chat.id, user: user, target: target })
            .then(res => {
                counter.inc();
                bot.sendMessage(msg.chat.id, `${user} slapped ${target}!`);
            });
        }
    }
}