const makeMention = require('../utils/makeMention');
const getTarget = require('../utils/getTarget');
const isAdmin = require('../utils/isAdmin');
const { makeEnabled } = require('../utils/admin');

module.exports =  {
    command: ['/enableslaps'],
    action: async (bot, msg, self, details) => {
        let target = getTarget(msg);
        let user = makeMention(msg.from.username);

        let admin = await isAdmin(bot, msg, msg.from.username);

        if (admin) {
            let enabled = await makeEnabled(msg.chat.id, target);
            bot.sendMessage(msg.chat.id, `${user} has restored the slapping priveleges of ${target}.`);
        } else {
            bot.sendMessage(msg.chat.id, `You aren't allowed to do that ${user}.`);
        }
    }
}