module.exports =  {
    command: ['/help', '/start'],
    action: (bot, msg, props) => {
        const helpText = `SlapBot is a pointless little bot that allows you to virtually slap other people in a group. Slapping can be done with a mention or by replying to something that was said.
        
Commands:
/help - Show this text.
/slap [user] - Slap a user. Example: \`/slap @someuser\` or respond with \`/slap\`
/slapcount [user] - Display how many times a user has been slapped.

Admins can use:
/disableslaps [user] - Disable a user's ability to use @SuperSlapBot
/enableslaps [user] - Enable a user's ability to @SuperSlapBot 
/slappermissions [user] - Check if a user can use @SuperSlapBot`;
    
        bot.sendMessage(msg.from.id, helpText);
    }
}