const getTarget = require('../utils/getTarget');
const Slap = require('../db/models').Slap;

module.exports =  {
    command: ['/slapcount'],
    action: (bot, msg, self, details) => {
        let target = getTarget(msg);
        Slap.count({
            where: {
                chatId: msg.chat.id,
                target: target
            }
        })
        .then(c => {
            bot.sendMessage(msg.chat.id, `${target} has been slapped ${c} times!`);
        });
    }
}