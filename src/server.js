const Telebot = require('telebot');
const commands = require('./commands');
const newSlap = require('./utils/newSlap');
const makeMention = require('./utils/makeMention');

const environment = require('./utils/environment');
if (environment == 'development') require('dotenv').config();

const bot = new Telebot({
    token: process.env.TOKEN,
    polling: {
        interval: 200,
        timeout: 0
    }
});

Object.keys(commands).forEach(key => {
    let command = commands[key];
    bot.on(command.command, (msg, self, details) => command.action(bot, msg, self, details));
    console.log('Loaded command ', key);
});

bot.on('newChatMembers', (msg, self, details) => {
    msg.reply.photo('http://www.linkhumans.fr/wp-content/uploads/2016/04/batman-slap-robin.png', { asReply: true });
});

bot.start();