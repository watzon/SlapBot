const Slap = require('../db/models').Slap;

module.exports = exports = (bot, msg, user, target) => {
    if (target === user) {
        bot.sendMessage(msg.chat.id, `Why are you hitting yourself, ${user}?`);
    } else if (target === '@SuperSlapBot') {
        bot.sendMessage(msg.chat.id, 'Why would you hit me? I thought we were friends.');
    } else {
        Slap.create({ chatId: msg.chat.id, user: user, target: target })
            .then(res => {
                bot.sendMessage(msg.chat.id, `${user} slapped ${target}!`);
            });
    }
}