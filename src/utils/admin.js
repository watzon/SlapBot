const SlapDisabledUsers = require('../db/models').SlapDisabledUsers;

const isDisabled = async (chatId, username) => {
    let res = await SlapDisabledUsers.count({
        where: { chat_id: chatId, user: username }
    })
    .then(count => {
        if (count > 0) {
            return true;
        }
    
        return false;
    });

    return res;
}

const makeDisabled = async (chatId, username) => {
    let disabled = await isDisabled(chatId, username);
    if (disabled) return;

    let newlyDisabled = await SlapDisabledUsers.create({
        chat_id: chatId,
        user: username
    });

    return newlyDisabled;
}

const makeEnabled = async (chatId, username) => {
    let disabled = await isDisabled(chatId, username);
    if (!disabled) return;

    console.log(chatId, username);

    await SlapDisabledUsers.findOne({ where: {
            chat_id: chatId,
            user: username
        }
    }).then(u => u.destroy());
}

module.exports = exports = {
    isDisabled: isDisabled,
    makeDisabled: makeDisabled,
    makeEnabled: makeEnabled
}