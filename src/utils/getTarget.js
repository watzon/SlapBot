const makeMention = require('./makeMention');

module.exports = exports = (msg) => {
    const pieces = msg.text.split(/\s+/);
    if (msg.reply_to_message) {
        return makeMention(msg.reply_to_message.from.username);
    } else if (pieces[1]) {
        return pieces[1][0] === '@' ? pieces[1] : makeMention(pieces[1]);
    } else {
        return makeMention(msg.from.username);
    }
}