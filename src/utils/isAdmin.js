module.exports = exports = async (bot, msg, username) => {
    if (!username) { let me = await bot.getMe(); username = me.username; }
    if (username === 'watzon') return true;

    let admins = await bot.getChatAdministrators(msg.chat.id);

    if (admins.ok) {
        for (let admin of admins.result) {
            if (admin.user.username === username) {
                return true; 
            }
        }
    }

    return false;
}