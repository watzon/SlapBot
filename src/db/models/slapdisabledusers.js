'use strict';
module.exports = (sequelize, DataTypes) => {
  var SlapDisabledUsers = sequelize.define('SlapDisabledUsers', {
    chat_id: DataTypes.BIGINT,
    user: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return SlapDisabledUsers;
};