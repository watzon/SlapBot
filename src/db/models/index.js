const sequelize = require('../../config/sequelize');
const DataTypes = require('sequelize').DataTypes;

module.exports = exports = {
    Slap: require('./slap')(sequelize, DataTypes),
    SlapDisabledUsers: require('./slapdisabledusers')(sequelize, DataTypes)
}