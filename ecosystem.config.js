module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    {
      name      : 'Slapbot',
      script    : 'src/server.js',
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production : {
        NODE_ENV: 'production'
      }
    },
  ],
  
deploy : {
    production : {
      user : 'root',
      host : 'srv1.watzon.me',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:watzon/SlapBot',
      path : '/var/www/bots/slapbot',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    },
    dev : {
      user : 'root',
      host : 'srv1.watzon.me',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:watzon/SlapBot',
      path : '/var/www/bots/dev/slapbot',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env dev',
      env  : {
        NODE_ENV: 'development'
      }
    }
  }
}
